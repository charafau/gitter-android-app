package im.gitter.gitter.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import im.gitter.gitter.LoginData;
import im.gitter.gitter.R;
import im.gitter.gitter.notifications.RegistrationData;
import im.gitter.gitter.notifications.RegistrationIntentService;

public class StartActivity extends AppCompatActivity {

    private static final int LOGIN_ACTIVITY_RESULT_ID = 1337;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LoginData loginData;
    private RegistrationData registrationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginData = new LoginData(this);
        registrationData = new RegistrationData(this);

        if (loginData.isLoggedIn()) {

            if (!registrationData.hasRegistered()) {
                registerForNotifications();
            }

            startActivity(new Intent(this, MainActivity.class));

            // stop users from navigating back to this activity
            finish();
        } else {
            setContentView(R.layout.activity_start);
        }
    }

    public void onGithubLoginClick(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("auth_provider", "github");
        startActivityForResult(intent, LOGIN_ACTIVITY_RESULT_ID);
    }

    public void onTwitterLoginClick(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("auth_provider", "twitter");
        startActivityForResult(intent, LOGIN_ACTIVITY_RESULT_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_ACTIVITY_RESULT_ID) {
            if (resultCode == RESULT_OK) {
                // store credentials
                String userId = data.getStringExtra("userId");
                String accessToken = data.getStringExtra("accessToken");
                loginData.setLoginData(userId, accessToken);

                registerForNotifications();

                startActivity(new Intent(this, MainActivity.class));
                // stop users from navigating back to this activity
                finish();
            }
        }
    }

    private void registerForNotifications() {
        if(checkPlayServices()) {
            startService(new Intent(this, RegistrationIntentService.class));
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.e("gitter", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
