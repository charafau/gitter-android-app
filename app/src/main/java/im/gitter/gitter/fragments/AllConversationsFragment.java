package im.gitter.gitter.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import im.gitter.gitter.R;
import im.gitter.gitter.RoomListAdapter;
import im.gitter.gitter.RoomListItemSelectListener;
import im.gitter.gitter.SearchController;
import im.gitter.gitter.SearchResultsAdapter;
import im.gitter.gitter.activities.CreateCommunityActivity;
import im.gitter.gitter.activities.CreateRoomActivity;
import im.gitter.gitter.activities.MainActivity;
import im.gitter.gitter.content.ContentProvider;
import im.gitter.gitter.content.RestResponseReceiver;
import im.gitter.gitter.content.RestServiceHelper;
import im.gitter.gitter.models.RoomListItem;
import im.gitter.gitter.models.Suggestion;
import im.gitter.gitter.network.ApiModelListRequest;
import im.gitter.gitter.network.VolleySingleton;

public class AllConversationsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, LoaderManager.LoaderCallbacks<Cursor>, RoomListItemSelectListener, SearchController.Listener, View.OnClickListener {

    private static final int LOADER_ID = 1000;
    private static final int CREATE_ROOM_ACTIVITY_RESULT_ID = 2000;
    private static final int CREATE_COMMUNITY_ACTIVITY_RESULT_ID = 3000;

    private MainActivity activity;
    private SwipeRefreshLayout swipeLayout;
    private View view;

    private RecyclerView recyclerView;
    private SearchView searchView;
    private String lastQuery = "";
    private RestResponseReceiver networkResponseReceiver = new RestResponseReceiver();
    private RoomListAdapter roomListAdapter;
    private SearchResultsAdapter searchResultsAdapter;
    private SearchController searchController;
    private FloatingActionsMenu floatingActionsMenu;
    private FloatingActionButton createCommunityButton;
    private FloatingActionButton createRoomButton;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            this.activity = (MainActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must be MainActivity");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        roomListAdapter = new RoomListAdapter(getActivity(), this);
        searchResultsAdapter = new SearchResultsAdapter(getActivity(), this);
        searchController = new SearchController(getActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().setTitle(R.string.all_conversations);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_all_conversations, container, false);

            swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
            swipeLayout.setOnRefreshListener(this);
            swipeLayout.setColorSchemeResources(R.color.caribbean, R.color.jaffa, R.color.ruby);

            recyclerView = (RecyclerView) view.findViewById(R.id.list_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(roomListAdapter);

            floatingActionsMenu = (FloatingActionsMenu) view.findViewById(R.id.add_button);
            createCommunityButton = (FloatingActionButton) view.findViewById(R.id.create_community_button);
            createCommunityButton.setOnClickListener(this);
            createRoomButton = (FloatingActionButton) view.findViewById(R.id.create_room_button);
            createRoomButton.setOnClickListener(this);
        }

        getActivity().registerReceiver(networkResponseReceiver, networkResponseReceiver.getFilter());

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.room_list, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        if (lastQuery.length() > 0) {
            // we are now restoring the last search

            // show the search text entry point but set query listener after expansion
            // as expansion would trigger a search for "" by default
            searchView.setOnQueryTextListener(null);
            searchMenuItem.expandActionView();
            searchView.setOnQueryTextListener(this);

            searchView.setQuery(lastQuery, false);
        } else {
            searchView.setOnQueryTextListener(this);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
        }

        RestServiceHelper.getInstance().getRoomList(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();

        // stop listening to the searchview otherwise it starts empty searches for ""
        // during the menu destruction when another fragment takes its place
        if (searchView != null) {
            searchView.setOnQueryTextListener(null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                activity.openDrawer();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onRefresh() {
        long requestId = RestServiceHelper.getInstance().getRoomList(getActivity());

        networkResponseReceiver.listen(requestId, new RestResponseReceiver.SuccessFailureListener() {
            @Override
            public void onSuccess() {
                swipeLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode) {
                swipeLayout.setRefreshing(false);
                Toast.makeText(
                        getActivity(),
                        R.string.network_failed,
                        Toast.LENGTH_SHORT
                ).show();
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return onQueryTextChange(query);
    }

    @Override
    public boolean onQueryTextChange(String query) {
        if (query.length() > 0) {
            recyclerView.setAdapter(searchResultsAdapter);
            swipeLayout.setEnabled(false);
            searchController.search(query);
        } else {
            searchController.clear();
            recyclerView.setAdapter(roomListAdapter);
            swipeLayout.setEnabled(true);
        }

        lastQuery = query;

        return false;
    }

    @Override
    public void onClick(View view) {
        if (view == createCommunityButton) {
            floatingActionsMenu.collapseImmediately();
            Intent intent = new Intent(getActivity(), CreateCommunityActivity.class);
            startActivityForResult(intent, CREATE_COMMUNITY_ACTIVITY_RESULT_ID);
        } else if (view == createRoomButton) {
            floatingActionsMenu.collapseImmediately();
            Intent intent = new Intent(getActivity(), CreateRoomActivity.class);
            startActivityForResult(intent, CREATE_ROOM_ACTIVITY_RESULT_ID);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CREATE_COMMUNITY_ACTIVITY_RESULT_ID) {
            if (resultCode == Activity.RESULT_OK && data.hasExtra(CreateCommunityActivity.RESULT_EXTRA_ROOM_URI)) {
                String roomUri = data.getStringExtra(CreateCommunityActivity.RESULT_EXTRA_ROOM_URI);
                activity.createOrJoinRoom(roomUri);
            }
        } else if (requestCode == CREATE_ROOM_ACTIVITY_RESULT_ID) {
            if (resultCode == Activity.RESULT_OK) {
                String roomId = data.getStringExtra(CreateRoomActivity.RESULT_EXTRA_ROOM_ID);

                Intent intent = new Intent(activity, MainActivity.class);
                intent.putExtra(MainActivity.GO_TO_ROOM_ID_INTENT_KEY, roomId);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                activity.startActivity(intent);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                if (data != null && data.getBooleanExtra(CreateRoomActivity.RESULT_EXTRA_COMMUNITY_REQUIRED, false)) {
                    Intent intent = new Intent(getActivity(), CreateCommunityActivity.class);
                    startActivityForResult(intent, CREATE_COMMUNITY_ACTIVITY_RESULT_ID);
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(networkResponseReceiver);
        if (view != null) {
            ViewGroup parentViewGroup = (ViewGroup) view.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ContentProvider.USER_ROOMS_CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        roomListAdapter.setCursor(cursor);
        updateSuggestedRooms();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        roomListAdapter.setCursor(null);
    }

    @Override
    public void onSelect(RoomListItem roomListItem) {
        String roomId = roomListItem.getRoomId();
        if (roomId != null) {
            Intent intent = new Intent(activity, MainActivity.class);
            intent.putExtra(MainActivity.GO_TO_ROOM_ID_INTENT_KEY, roomId);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            activity.startActivity(intent);
        } else {
            activity.createOrJoinRoom(roomListItem.getUri());
        }
    }

    private void updateSuggestedRooms() {
        VolleySingleton.getInstance(activity).getRequestQueue().add(new ApiModelListRequest<Suggestion>(
                activity,
                "/v1/user/me/suggestedRooms",
                new Response.Listener<ArrayList<Suggestion>>() {
                    @Override
                    public void onResponse(ArrayList<Suggestion> response) {
                        roomListAdapter.setSuggestedRooms(response);
                    }
                },
                null
        ) {
            @Override
            protected Suggestion createModel(JSONObject jsonObject) throws JSONException {
                return new Suggestion(jsonObject);
            }
        }.setTag(activity));
    }


    @Override
    public void onResults(List<RoomListItem> results) {
        searchResultsAdapter.setResults(results);
    }

    @Override
    public void onError() {
        Toast.makeText(
                getActivity(),
                R.string.network_failed,
                Toast.LENGTH_SHORT
        ).show();
    }
}
